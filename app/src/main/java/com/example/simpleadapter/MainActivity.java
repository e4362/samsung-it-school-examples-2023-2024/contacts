package com.example.simpleadapter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.list_view);
        String[] names = {"Rozetked", "Вилсаком", "NN",
                "Дурка"
        };
        int[] photoIds = {R.drawable.image1, R.drawable.image2,
                R.drawable.image3,
                R.drawable.image1
        };
        String[] URLs = {"tg://resolve?domain=rozetked",
                "tg://resolve?domain=Wylsared",
                "tg://resolve?domain=naebnet",
                "https://t.me/+Kit4wFdfQNBhMjVi"
        };

        ArrayList<HashMap<String, Object>> data = new ArrayList<>();

        for (int j = 0; j < 10; j++){
            for (int i = 0; i < names.length; i++) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("name", names[i]);
                map.put("photo", photoIds[i]);
                map.put("url", URLs[i]);
                data.add(map);
            }
        }
        String[] from = {"name", "photo"};
        int[] to = {R.id.textView, R.id.imageView};
        SimpleAdapter adapter = new SimpleAdapter(this, data,
                R.layout.item_list, from, to);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String url = (String) data.get(position).get("url");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }
        });

    }
}